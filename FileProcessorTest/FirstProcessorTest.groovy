package FileProcessorTest

import FileProcessor.FileProcessor
import FileProcessor.FirstProcessor
import spock.lang.Specification

class FirstProcessorTest extends Specification {
    FirstProcessor firstProcessor = new FirstProcessor()
    File file = new File("/home/nikl_pod/IdeaProjects/GeekHub/src/done_a_h1")

    def "File is null"(){
        expect:
            firstProcessor.file.equals(null)
    }

    def "File exists"(){
        setup:
            firstProcessor.setFile(file)

        expect:
            firstProcessor.file.exists()
    }

    def "Number of character 'a' in a file"(){
        setup:
            int i = 0
            file.eachLine {
                line -> line.eachMatch("a", { i++ })
            }

        expect:
            i == 20

    }

    def "write in file new line"(){
        setup:
            firstProcessor.setFile(file)
            int oldLine = 0
            file.eachLine {oldLine++}
            firstProcessor.writeInFile()
            int newLine = 0
            file.eachLine {newLine++}

        expect:
            oldLine < newLine
    }
}