package FileProcessorTest

import FileProcessor.SecondProcessor
import spock.lang.Specification

import java.lang.reflect.Array

/**
 * Created by nikl_pod on 20.11.14.
 */
class SecondProcessorTest extends Specification{
    def "generate matrix and diagonals"(){
        setup:
            def sumOfMainDiagonal = 0
            def sumOfAntiDiagonal = 0
            def matrix = new Object[4][4]
            Random random = new Random()
            (0..<matrix.length).each { i->
                (0..<matrix[i].length).each { j->
                    matrix[i][j] = random.nextInt(99)
                    if (i == j) {
                        sumOfMainDiagonal += matrix[i][j]
                    }
                    if ((i + j) == matrix.length - 1) {
                        sumOfAntiDiagonal += matrix[i][j]
                    }

                }
            }

            def expectedSumOfMainDiagonal = 108
            def expectedSumOfAntiDiagonal = 200

        expect:
            sumOfMainDiagonal == expectedSumOfMainDiagonal
            sumOfAntiDiagonal == expectedSumOfAntiDiagonal

    }
}
