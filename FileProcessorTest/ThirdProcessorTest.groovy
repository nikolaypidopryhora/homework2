package FileProcessorTest

import spock.lang.Specification

/**
 * Created by nikl_pod on 20.11.14.
 */
class ThirdProcessorTest extends Specification{
    
    def "Date in file and curent date" (){
         setup:
              File file = new File("/home/nikl_pod/IdeaProjects/GeekHub/src/Files/d_d1")
              def dateInFile = file.getText()
              String curentDate = new Date().format("​yyyy-MM-dd HH-mm-ss")
         expect:
              dateInFile < curentDate
    }
}
