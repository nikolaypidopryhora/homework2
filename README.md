# HomeWork2 #

Write script which will process files in folder (folder path should be passed to script as a parameter).
 - each file which name started with "a_" should calculate number of "a" character in it and write result as a new line to file
 - each file which starts from "1_" should replace file content with randomly generated matrix 4 x 4 and sum of main diagonal and antidiagonal from new line
 - each file which starts from "d_" should replace file content with date in "YYYY-MM-DD HH-MM-SS" format
 - other file should be ignored

All processed files should be renamed with next format: "done_" + old name
