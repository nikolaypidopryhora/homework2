package FileProcessor

/**
 * Created by nikl_pod on 20.11.14.
 */
class FirstProcessor {
    private File file

    void setFile(file){
        this.file = file
    }

    private int countA(){
        int i = 0;
        file.eachLine {
            line -> line.eachMatch("a", { i++ })
        }
        i
    }

    void renameFile(){
        file.renameTo file.parent + "/done_" + file.name
    }

    void writeInFile(){
        file.append("\nNumber of \"a\" is: " + countA())
    }
}
