package FileProcessor

/**
 * Created by nikl_pod on 20.11.14.
 */
class FileProcessor {

    FileProcessor(String path){
        process(path)
        println("All done")
    }

    private void process(path){
        def dir = new File(path)

        FirstProcessor firstProcessor = new FirstProcessor()
        SecondProcessor secondProcessor = new SecondProcessor()
        ThirdProcessor thirdProcessor = new ThirdProcessor()

        dir.eachFile { file ->
            if (file.name.startsWith("a_")){
                firstProcessor.setFile(file)
                firstProcessor.writeInFile()
                firstProcessor.renameFile()
            }
            if (file.name.startsWith("1_")) {
                secondProcessor.setFile(file)
                secondProcessor.generateMatrix()
                secondProcessor.writeMatrixInFile()
                secondProcessor.writeMainDiagonalInFile()
                secondProcessor.writeAntiDiagonalInFile()
                secondProcessor.renameFile()
            }
            if (file.name.startsWith("d_")) {
                thirdProcessor.setFile(file)
                thirdProcessor.writeDateInFile()
                thirdProcessor.renameFile()
            }
        }
    }
}

new FileProcessor('/home/nikl_pod/IdeaProjects/GeekHub/src/Files/')
