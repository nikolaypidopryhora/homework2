package FileProcessor

import java.lang.reflect.Array

/**
 * Created by nikl_pod on 20.11.14.
 */
class SecondProcessor {
    private File file
    private def array

    void setFile(file){
        this.file = file
    }

    void generateMatrix(){
        array = new Object[4][4]
        def random = new Random();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                array[i][j] = random.nextInt(99)
            }
        }
    }

    int sumOfMainDiagonal(){
        if (!array) return -1
        def sumOfMainDiagonal = 0
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (i==j){
                    sumOfMainDiagonal += array[i][j]
                }
            }
        }
        sumOfMainDiagonal
    }

    int sumOfAntiDiagonal(){
        if (!array) return -1
        def sumOfAntiDiagonal = 0
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if ((i+j) == array.length - 1){
                    sumOfAntiDiagonal += array[i][j]
                }
            }
        }
        sumOfAntiDiagonal
    }

    void writeMatrixInFile() {
        file.write("")
        def separator = "|"
        for (int i = 0; i < 4; i++) {
            def line = separator
            for (int j = 0; j < 4; j++) {
                line += array[i][j] + separator
            }
            file << line + "\n"
        }
    }

    void writeMainDiagonalInFile(){
        file << "\nSum of main diagonal = ${sumOfMainDiagonal()}"
    }

    void writeAntiDiagonalInFile(){
        file << "\nSum of antidiagonal = ${sumOfAntiDiagonal()}\n"
    }

    void renameFile(){
        file.renameTo file.parent + "/done_" + file.name
    }
}
