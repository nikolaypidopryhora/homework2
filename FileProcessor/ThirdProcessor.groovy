package FileProcessor

/**
 * Created by nikl_pod on 20.11.14.
 */
class ThirdProcessor {
    private File file

    void setFile(file){
        this.file = file
    }

    void writeDateInFile(){
        file.write(new Date().format("​yyyy-MM-dd HH-mm-ss"))
    }

    void renameFile(){
        file.renameTo file.parent + "/done_" + file.name
    }
}
